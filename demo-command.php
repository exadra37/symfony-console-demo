<?php

require __DIR__.'/vendor/autoload.php';

use Exadra37\SymfonyConsoleDemo\Command\GreetCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new GreetCommand());
$application->run();
